function GameDate(seconds) {
  this.seconds = seconds; // since 1 January 1970

  this.addTraits = function(object) {
    object.represent = function() {
      var year = 1970 + Integer.parseInt(this.seconds / (60.0 * 60.0 * 24.0 * 365.0));
      var day = Integer.parseInt(this.seconds / (60.0 * 60.0 * 24.0)) % 365;
      var hours = Integer.parseInt(this.seconds / (60.0 * 60.0)) % 24;
      var minutes = Integer.parseInt(this.seconds / 60.0) % 60;
      return year + " Day " + day + " " + hours + ":" + minutes;
    };

    object.spendHours = function(hours) {
      this.seconds += 60.0 * 60.0 * hours;
    };

    object.spendHour = function() {
      this.seconds += 60.0 * 60.0;
    };

    object.spendHalfHour = function() {
      this.seconds += 60.0 * 30.0;
    };

    object.spendMinute = function() {
      this.seconds += 60 * 60;
    };

    object.daysSince = function(date) {
      var diff = this.seconds - date.seconds;
      var days = Integer.parseInt(diff / (60.0 * 60.0 * 24.0));
      return days;
    };

    object.copy = function() {
      return new GameDate(this.seconds);
    };
  };

  this.addTraits(this);
};
