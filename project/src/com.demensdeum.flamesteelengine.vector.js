function Vector() {
  this.x = 0;
  this.y = 0;
  this.z = 0;

  this.addTraits = function(object) {
    object.printout = function(prefix) {
      print(prefix + " x = " + this.x + "; y = " + this.y + "; z = " + this.z);
    };

    object.copy = function() {
      return newVector(this.x, this.y, this.z);
    };

    object.asRadians = function() {
      return newVector(
        this.x * 0.017453298768179,
        this.y * 0.017453298768179,
        this.z * 0.017453298768179
      );
    };

    object.swapXY = function() {
      var x = this.x;
      var y = this.y;
      this.x = y;
      this.y = x;
      return this;
    };

    object.asAngles = function() {
      return newVector(
        this.x * 57.2958,
        this.y * 57.2958,
        this.z * 57.2958
      );
    };
  };

  this.addTraits(this);
};

function newVector(x, y, z) {
  var vector = new Vector();
  vector.x = x;
  vector.y = y;
  vector.z = z;
  return vector;
};

function newVectorFromCopy(vector) {
  return newVector(vector.x, vector.y, vector.z);
}

function sumVectors(lhs, rhs) {
  var vector = new Vector();
  vector.x = lhs.x + rhs.x;
  vector.y = lhs.y + rhs.y;
  vector.z = lhs.z + rhs.z;
  return vector;
}

function multipyVectorScalar(vector, scalar) {
    var vectorCopy = newVectorFromCopy(vector);
    vectorCopy.x = vectorCopy.x * scalar;
    vectorCopy.y = vectorCopy.y * scalar;
    vectorCopy.z = vectorCopy.z * scalar;
    return vectorCopy;
}
