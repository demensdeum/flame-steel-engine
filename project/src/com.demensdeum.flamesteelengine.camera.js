function translateCamera(object, x, y, z) {

  var position = object.position;
  var rotation = object.rotation;

  var yaw = radians(-90 - rotation.x);
  var pitch = radians(-rotation.y);

  var front = new Vector();
  front.x = Math.cos(pitch) * Math.cos(yaw);
  front.y = Math.sin(pitch);
  front.z = Math.cos(pitch) * Math.sin(yaw);

  var cameraUp = newVector(0.0, 1.0,  0.0);
  var right = normalizeVector(crossVectors(front, cameraUp));

  var cameraFront = normalizeVector(front);

  var a = multipyVectorScalar(cameraFront, z);
  var b = multipyVectorScalar(right, x);

  var positionDiff = sumVectors(a, b);

  var targetPosition = sumVectors(position, positionDiff);
  targetPosition.y += y;

  position.x = targetPosition.x;
  position.y = targetPosition.y;
  position.z = targetPosition.z;

};
