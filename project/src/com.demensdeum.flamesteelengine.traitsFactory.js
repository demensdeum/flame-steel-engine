function TraitsFactory() {

  this.addSceneObjectTraits = function(object, sceneObject) {
    object.linkedSceneObjectName = sceneObject.name;
    object.sceneObject = function() {
      return getObject(this.linkedSceneObjectName);
    };
  };

};
