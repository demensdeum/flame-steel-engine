function Context() {

  this.gameplayData = null;
  this.currentController = null;

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      print("Context initialized");
      setWindowTitle(GLOBAL_APP_TITLE);
      this.gameplayData = new GameplayData();
      this.initializeGame();
      this.initialized = true;
    }
  };

  this.initializeGame = function() {

  };

  this.step = function() {
    this.initializeIfNeeded();
    if (this.currentController == null) {
      print("Context can't run with empty current controller");
      exit(1);
    }
    else {
      this.currentController.step();
    }
  };

  this.switchToController = function(controller) {
    controller.delegate = this;
    controller.gameplayData = this.gameplayData;
    this.currentController = controller;
  };
};
