BINDINGS_DEBUG = false;

function createObject() {
    var object = {
        playAnimation : function(animationName) {
            playAnimation__private(this.name, animationName);
        }
    };

    var position = new Vector();
    position.x = 0;
    position.y = 0;
    position.z = 0;

    var rotation = new Vector();
    rotation.x = 0;
    rotation.y = 0;
    rotation.z = 0;

    var scale = new Vector();
    scale.x = 1;
    scale.y = 1;
    scale.z = 1;

    object.position = position;
    object.rotation = rotation;
    object.scale = scale;

    object.surfaceMaterialEnabled = false;
    object.surfaceWidth = 0;
    object.surfaceHeight = 0;

    object.layer = "Scene";

    return object;
}

function clearSurfaceOfObject(object) {
  clearSurfaceOfObject__private(object.name);
};

function drawTextIntoObjectRect(
  text,
  object,
  rectangle
)
{
  var objectName = object.name;
  var dX = rectangle.x;
  var dY = rectangle.y;
  drawTextIntoObjectRect__private(text, objectName, dX, dY);
};

function drawImageRectIntoObjectRect(
  image,
  sourceRectangle,
  object,
  destinationRectangle
)
{
  var objectName = object.name;
  drawImageRectIntoObjectRect__private(
    image,
    sourceRectangle.x,
    sourceRectangle.y,
    sourceRectangle.width,
    sourceRectangle.height,
    objectName,
    destinationRectangle.x,
    destinationRectangle.y,
    destinationRectangle.width,
    destinationRectangle.height
  );
};

function getObject(name) {
    getObject__private(name);
    var output = getObject__private__CallResult;

    var vector = new Vector();
    vector.addTraits(output.position);
    vector.addTraits(output.rotation);
    vector.addTraits(output.scale);

    return output;
};

function addObject(object) {
    var x = object.position.x;
    var y = object.position.y;
    var z = object.position.z;

    var rX = object.rotation.x;
    var rY = object.rotation.y;
    var rZ = object.rotation.z;

    var sX = object.scale.x;
    var sY = object.scale.y;
    var sZ = object.scale.z;

    var surfaceMaterialEnabled = object.surfaceMaterialEnabled;
    var surfaceWidth = object.surfaceWidth;
    var surfaceHeight = object.surfaceHeight;

    var layer = object.layer;

    addObject__private(
      object.name,
      object.modelPath,
      x, y, z,
      rX, rY, rZ,
      sX, sY, sZ,
      surfaceMaterialEnabled, surfaceWidth, surfaceHeight,
      layer
    );
}

function updateObject(object) {
    var x = object.position.x;
    var y = object.position.y;
    var z = object.position.z;

    var rX = object.rotation.x;
    var rY = object.rotation.y;
    var rZ = object.rotation.z;

    var sX = object.scale.x;
    var sY = object.scale.y;
    var sZ = object.scale.z;

    if (BINDINGS_DEBUG) {
        print("!!!update object: " + object.name +
        " x: " + x +
        " y: " + y +
        " z: " + z +
        " rX: " + rX +
        " rY: " + rY +
        " rZ: " + rZ +
        " sX: " + sX +
        " sY: " + sY +
        " sZ: " + sZ
      );
    }

    updateObject__private(object.name, x, y, z, rX, rY, rZ, sX, sY, sZ);
};

function removeAllObjects() {
    removeAllObjects__private();
}
