function debugConsole() {
  var command = " ";
  while (command.length > 0) {
    command = prompt("--- Debug Console ---");
    eval(command);
  }
}

function degrees(radians) {
  return radians * 57.2958;
}

function uuid() {
  var length = Math.randInt(30, 60);
  var dictionary = ["aAc", "qw", "b", "c", "d", "e", "f", "sdf", "qwexc", "asdas", "1212", "x", "ar", "sux", "dage", "stan", "sila"];
  var output = "";
  for (var i = 0; i < length; i++) {
    output += dictionary[Math.randInt(0, dictionary.length - 1)];
  }
  return output;
}

function distance2dBetweenObjects(objectNameA, objectNameB) {
  var objectA = getObject(objectNameA);
  var objectB = getObject(objectNameB);
  var distance = Math.sqrt(Math.pow((objectB.position.x - objectA.position.x), 2) + Math.pow((objectB.position.z - objectA.position.z), 2));
  print("Distance: " + distance);
  return distance;
}

function printVector(vector) {
  print("Vector: x: " + vector.x + " y: " + vector.y + " z: " + vector.z);
}

function printMatrix(matrix) {
  print("Print Matrix");
  var outputString = "\n";
  for (var x = 0; x < matrix.length; x++) {
    var row = matrix[x];
    for (var y = 0; y < row.length; y++) {
      outputString = outputString + row[y] + ",";
    }
    outputString = outputString + "\n";
  }
  print(outputString);
}

function copyVector(vector) {
  var outputVector = new Object();
  outputVector.x = vector.x;
  outputVector.y = vector.y;
  outputVector.z = vector.z;
  return outputVector;
}

function normalizeVector(vector) {
  var vectorCopy = vector;
  var w = Math.sqrt(vectorCopy.x * vectorCopy.x + vectorCopy.y * vectorCopy.y + vectorCopy.z * vectorCopy.z);
  vectorCopy.x = vectorCopy.x / w;
  vectorCopy.y = vectorCopy.y / w;
  vectorCopy.z = vectorCopy.z / w;
  return vectorCopy;
}

function crossVectors(lhsVector, rhsVector) {
  var outputVector = new Vector();
  outputVector.x = lhsVector.y * rhsVector.z - lhsVector.z * rhsVector.y;
  outputVector.y = lhsVector.z * rhsVector.x - lhsVector.x * rhsVector.z;
  outputVector.z = lhsVector.x * rhsVector.y - lhsVector.y * rhsVector.x;
  return outputVector;
}

function dotVectors(l, r) {
  var o = l.x * r.x + l.y * r.y + l.z * r.z;
  return o;
}

function mat4MulMat4(l, r) {

  var m = [];
  m[0] = [0, 0, 0, 0];
  m[1] = [0, 0, 0, 0];
  m[2] = [0, 0, 0, 0];
  m[3] = [0, 0, 0, 0];

  m[0][0] = l[0][0] * r[0][0] + l[0][1] * r[1][0] + l[0][2] * r[2][0] + l[0][3] * r[3][0];
  m[0][1] = l[0][0] * r[0][1] + l[0][1] * r[1][1] + l[0][2] * r[2][1] + l[0][3] * r[3][1];
  m[0][2] = l[0][0] * r[0][2] + l[0][1] * r[1][2] + l[0][2] * r[2][2] + l[0][3] * r[3][2];
  m[0][3] = l[0][0] * r[0][3] + l[0][1] * r[1][3] + l[0][2] * r[2][3] + l[0][3] * r[3][3];

  m[1][0] = l[1][0] * r[0][0] + l[1][1] * r[1][0] + l[1][2] * r[2][0] + l[1][3] * r[3][0];
  m[1][1] = l[1][0] * r[0][1] + l[1][1] * r[1][1] + l[1][2] * r[2][1] + l[1][3] * r[3][1];
  m[1][2] = l[1][0] * r[0][2] + l[1][1] * r[1][2] + l[1][2] * r[2][2] + l[1][3] * r[3][2];
  m[1][3] = l[1][0] * r[0][3] + l[1][1] * r[1][3] + l[1][2] * r[2][3] + l[1][3] * r[3][3];

  m[2][0] = l[2][0] * r[0][0] + l[2][1] * r[1][0] + l[2][2] * r[2][0] + l[2][3] * r[3][0];
  m[2][1] = l[2][0] * r[0][1] + l[2][1] * r[1][1] + l[2][2] * r[2][1] + l[2][3] * r[3][1];
  m[2][2] = l[2][0] * r[0][2] + l[2][1] * r[1][2] + l[2][2] * r[2][2] + l[2][3] * r[3][2];
  m[2][3] = l[2][0] * r[0][3] + l[2][1] * r[1][3] + l[2][2] * r[2][3] + l[2][3] * r[3][3];

  m[3][0] = l[3][0] * r[0][0] + l[3][1] * r[1][0] + l[3][2] * r[2][0] + l[3][3] * r[3][0];
  m[3][1] = l[3][0] * r[0][1] + l[3][1] * r[1][1] + l[3][2] * r[2][1] + l[3][3] * r[3][1];
  m[3][2] = l[3][0] * r[0][2] + l[3][1] * r[1][2] + l[3][2] * r[2][2] + l[3][3] * r[3][2];
  m[3][3] = l[3][0] * r[0][3] + l[3][1] * r[1][3] + l[3][2] * r[2][3] + l[3][3] * r[3][3];

  return m;
}

function eulerRotationVectorFromMat4(m) {
  var r = new Object();
  r.z = Math.atan2(m[2][1], m[2][2]);
  r.y = Math.atan2(-m[2][0], Math.sqrt(m[2][1] * m[2][1] + m[2][2] * m[2][2]));
  r.x = Math.atan2(m[1][0], m[0][0]);
  return r;
}

function toRadians(angle) {
  return angle * 0.0174;
}

function vec3minusVec3(l, r) {

  var o = new Vector();
  o.x = l.x - r.x;
  o.y = l.y - r.y;
  o.z = l.z - r.z;

  return o;
}

function vec3MulMat4(vec, m) {

  var v = new Object();
  v.x = vec.x;
  v.y = vec.y;
  v.z = vec.z;
  v.w = 1;

  var result = [];
  result[0] = 0;
  result[1] = 0;
  result[2] = 0;
  result[3] = 1;

  for (var i = 0; i < 4; i++) {
    result[i] = v.x * m[0][i] + v.y * m[1][i] + v.z + m[2][i] + v.w * m[3][i];
  }

  result[0] = result[0] / result[3];
  result[1] = result[1] / result[3];
  result[2] = result[2] / result[3];

  var outputVector = newVector(result[0], result[1], result[2]);

  return outputVector;
}

function negateVector(vec) {
  var vector = vec.copy();
  vector.x = -vector.x;
  vector.y = -vector.y;
  vector.z = -vector.z;
  return vector;
}

function lookFromObjectToObject(fromObject, toObject) {
  var xDiff = fromObject.position.x - toObject.position.x;
  var yDiff = fromObject.position.y - toObject.position.y;
  var zDiff = fromObject.position.z - toObject.position.z;

  var diffVector = newVector(xDiff, yDiff, zDiff);
  diffVector = normalizeVector(diffVector);

  var y = Math.atan2(diffVector.x, diffVector.z) + radians(-90); // TODO: why -90?
  var x = Math.asin(diffVector.y);

  fromObject.rotation = newVector(0, y, x);
};

function lookAt(eye, target, up) {

  var m = [];
  m[0] = [1, 0, 0, 0];
  m[1] = [0, 1, 0, 0];
  m[2] = [0, 0, 1, 0];
  m[3] = [0, 0, 0, 1];

  var zAxis = normalizeVector(vec3minusVec3(target, eye));
  var xAxis = normalizeVector(crossVectors(zAxis, up));
  var yAxis = crossVectors(xAxis, zAxis);

  zAxis = negateVector(zAxis);

  m[0][0] = xAxis.x; m[1][0] = xAxis.y; m[2][0] = xAxis.z; m[3][0] = -dotVectors(xAxis, eye);
  m[0][1] = yAxis.x; m[1][1] = yAxis.y; m[2][1] = yAxis.z; m[3][1] = -dotVectors(yAxis, eye);
  m[0][2] = zAxis.x; m[1][2] = zAxis.y; m[2][2] = zAxis.z; m[3][2] = -dotVectors(zAxis, eye);
  m[0][3] = 0;       m[1][3] = 0;       m[2][3] = 0;       m[3][3] = 1;

  return m;
}



function positionVectorToFront(object, distance) {

  var cameraPosition = object.position;
  var cameraRotation = object.rotation;

  var yaw = degreesToRadians(-90 - cameraRotation.x);
  var pitch = degreesToRadians(0 - cameraRotation.y);

  var frontVector = new Object();
  frontVector.x = Math.cos(pitch) * Math.cos(yaw);
  frontVector.x = frontVector.x * distance;

  frontVector.y = Math.sin(pitch);
  frontVector.y = frontVector.y * distance;

  frontVector.z = Math.cos(pitch) * Math.sin(yaw);
  frontVector.z = frontVector.z * distance;

  var targetPosition = copyVector(cameraPosition);
  targetPosition.x += frontVector.x;
  targetPosition.y += frontVector.y;
  targetPosition.z += frontVector.z;

  var outputVector = new Object();

  outputVector.x = targetPosition.x;
  outputVector.y = targetPosition.y;
  outputVector.z = targetPosition.z;

  return outputVector;
}

function degreesToRadians(radians) {
  return radians * 0.01745;
}

function addDefaultCameraAtXYZAndRotationXYZ(x, y, z, rX, rY, rZ) {
  var camera = createObject();
  camera.name = "camera";

  var position = new Object();
  position.x = x;
  position.y = y;
  position.z = z;

  var rotation = new Object();
  rotation.x = rX;
  rotation.y = rY;
  rotation.z = rZ;

  camera.position = position;
  camera.rotation = rotation;

  addObject(camera);
}

function addDefaultCamera() {
  addDefaultCameraAtXYZAndRotationXYZ(0,0,0,0,0,0);
}

function radians(angle) {
  return angle * 0.0174533;
};

function translateObjectByRotation(object, x, y, z, yawDiff) {

  var position = object.position;
  var rotation = object.rotation;

  var yaw = -rotation.y + yawDiff;
  var pitch = rotation.z;

  var front = new Vector();
  front.x = Math.cos(pitch) * Math.cos(yaw);
  front.y = Math.sin(pitch);
  front.z = Math.cos(pitch) * Math.sin(yaw);

  var cameraUp = newVector(0.0, 1.0,  0.0);
  var right = normalizeVector(crossVectors(front, cameraUp));

  var cameraFront = normalizeVector(front);

  var a = multipyVectorScalar(cameraFront, z);
  var b = multipyVectorScalar(right, x);

  var positionDiff = sumVectors(a, b);

  var targetPosition = sumVectors(position, positionDiff);
  targetPosition.y += y;

  position.x = targetPosition.x;
  position.y = targetPosition.y;
  position.z = targetPosition.z;

};
