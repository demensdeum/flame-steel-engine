function Dictionary() {

  this.list = new List();

  this.addTraits = function(object) {

    var list = new List();
    list.addTraits(object.list);

    object.set = function(key, value) {
      this.list.removeMatchAll(function(keyValue) {
        return keyValue.key == key;
      });
      this.list.push(new DictionaryKeyValue(key, value));
    };

    object.get = function(key) {
      return this.list.getFirstMatch(function(keyValue) {
        return keyValue.key == key;
      }).value;
    };

    object.forEach = function(iterationFunction) {
      this.list.forEach(iterationFunction);
    };

    object.indexedForEach = function(iterationFunction) {
      this.list.indexedForEach(iterationFunction);
    };

    object.values = function() {
      var output = new List();
      this.list.forEach(function(node) {
        output.push(node.value);
      });
      return output;
    };

  };

  this.addTraits(this);

};
