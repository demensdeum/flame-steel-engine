DEBUG_WEBSOCKETS = true;

function SetWebSocketsDataHandler(dataHandler) {
  __global_webSocketsDataHandler = dataHandler;
}

function WebSocketsClient() {

  this.delegate = null;

  this.connect = function(address, delegate) {
    this.delegate = delegate;
    SetWebSocketsDataHandler(this);
    ConnectToWebSockets(address);
  };

  this.sendData = function(data) {
    if (DEBUG_WEBSOCKETS) {
      print(data);
    }
    SendDataToWebSockets(data);
  };

  this.didReceiveWebSocketsData = function(data) {
    if (DEBUG_WEBSOCKETS) {
      print("WebSocketsData: " + data);
    }

    this.delegate.webSocketsClientDidReceiveData(this, data);
  };

};
